Bootstrap for my machines. 

- [air](./air)

## Pre-requisite
- xcode
- homebrew
  - https://brew.sh/
- just (for justfile)
  - https://github.com/casey/just
- stow for dotfiles
  - https://www.gnu.org/software/stow/

## Download
Clone repo. Pull down the submodule

```
mkdir -p ~/src
git clone git@gitlab.com:rmchale/machine-setup.git ~/src/machine-setup 
cd src/machine-setup/libs
git submodule update --remote --init
```

## Install

```shell
xcode-select --install

/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
brew install just
brew install stow
```
